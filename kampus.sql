/*
SQLyog Community v13.1.5  (32 bit)
MySQL - 10.1.38-MariaDB : Database - kampus
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kampus` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kampus`;

/*Table structure for table `jenis_kelamin` */

DROP TABLE IF EXISTS `jenis_kelamin`;

CREATE TABLE `jenis_kelamin` (
  `id_jk` int(11) NOT NULL,
  `keterangan` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_jk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jenis_kelamin` */

insert  into `jenis_kelamin`(`id_jk`,`keterangan`) values 
(1,'Laki-laki'),
(2,'Perempuan');

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `nim` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `photos` varchar(20) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `id_jk` int(11) DEFAULT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`nim`,`nama`,`id_prodi`,`photos`,`alamat`,`id_jk`) values 
('1401001','Imam',1,'1.jpg','Kediri',1),
('1402001','Nurul',2,'2.jpg','Jombang',2),
('1403001','Mahmud',3,'3.jpg','Tulungagung',1),
('1501001','Khoirul',2,'4.jpg','Blitar',1),
('1502001','Junaedi',3,'5.jpg','Kediri',1),
('1503001','Ayu',1,'6.jpg','Nganjuk',2);

/*Table structure for table `prodi` */

DROP TABLE IF EXISTS `prodi`;

CREATE TABLE `prodi` (
  `id_prodi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_prodi` varchar(30) NOT NULL,
  PRIMARY KEY (`id_prodi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `prodi` */

insert  into `prodi`(`id_prodi`,`nama_prodi`) values 
(1,'Teknik Informatika'),
(2,'Mesin'),
(3,'Akutansi');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
